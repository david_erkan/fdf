/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:05:57 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			isometric(t_mlx *m)
{
	int		i;
	t_v3	*varray;

	if (!(varray = malloc(sizeof(t_v3) * m->vc->w * m->vc->h)))
		return (0);
	i = 0;
	while (i < m->vc->h)
	{
		ft_memcpy(varray + (i * m->vc->w),
			m->vc->tab[i], sizeof(t_v3) * m->vc->w);
		iso_3d_points_to_2d(m, varray, i);
		++i;
	}
	set_offset_step(m, varray);
	draw_varray(m, varray);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	free(varray);
	return (1);
}

int			parallel(t_mlx *m)
{
	int		i;
	t_v3	*varray;

	if (!(varray = malloc(sizeof(t_v3) * m->vc->w * m->vc->h)))
		return (0);
	i = 0;
	while (i < m->vc->h)
	{
		ft_memcpy(varray + (i * m->vc->w),
			m->vc->tab[i], sizeof(t_v3) * m->vc->w);
		parallel_3d_points_to_2d(m, varray, i);
		++i;
	}
	set_offset_step(m, varray);
	draw_varray(m, varray);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	free(varray);
	return (1);
}

int			render(t_mlx *m)
{
	if (m->mode == MODE_ISO)
		return (isometric(m));
	if (m->mode == MODE_PARALLEL)
		return (parallel(m));
	return (0);
}
