/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_rc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:07:58 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** Adds one more elements to the first dimension of t_rc::tab
*/

int			expand_tab(t_rc *rc)
{
	t_v3	**tab;

	tab = malloc(sizeof(t_v3 *) * rc->h + 1);
	if (!tab)
		return (0);
	ft_memcpy(tab, rc->tab, (size_t)(rc->h * sizeof(t_v3 *)));
	free(rc->tab);
	rc->tab = tab;
	return (1);
}

t_rc		*malloc_rc(void)
{
	t_rc	*rc;

	rc = malloc(sizeof(t_rc));
	if (!rc)
		return (NULL);
	rc->line = NULL;
	rc->numbers = NULL;
	rc->tab = NULL;
	rc->h = 0;
	rc->w = 0;
	return (rc);
}

void		*clean_rc(t_rc *rc)
{
	int		i;

	i = 0;
	if (!rc)
		return (NULL);
	if (rc->tab)
	{
		while (i < rc->h)
			free(rc->tab[i++]);
		free(rc->tab);
		rc->tab = NULL;
	}
	ft_memdel((void **)&rc->line);
	ft_memdel((void **)&rc->numbers);
	ft_memdel((void **)&rc);
	return (NULL);
}
