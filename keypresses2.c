/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keypresses2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:03:39 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			keypress_o_p(t_mlx *m, int key)
{
	if (key == P_KEY_LINUX && m->scale != 2147483647)
		m->scale += 1;
	if (key == O_KEY_LINUX && m->scale > 1)
		m->scale -= 1;
	zero_image(m, m->data);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	if (!render(m))
	{
		ft_putstr_fd("ERROR\n", 2);
		exit_fdf(m);
	}
	return (0);
}

int			keypress_i_j_k_l(t_mlx *m, int key)
{
	zero_image(m, m->data);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	if (key == I_KEY_LINUX)
		m->offset2.y += m->step.y;
	if (key == K_KEY_LINUX)
		m->offset2.y -= m->step.y;
	if (key == J_KEY_LINUX)
		m->offset2.x += m->step.x;
	if (key == L_KEY_LINUX)
		m->offset2.x -= m->step.x;
	if (!render(m))
	{
		ft_putstr_fd("ERROR\n", 2);
		exit_fdf(m);
	}
	return (0);
}
