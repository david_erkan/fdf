/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keypresses.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:03:20 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			keypress_esc(t_mlx *m)
{
	return (exit_fdf(m));
}

int			keypress_space(t_mlx *m)
{
	(void)m;
	zero_image(m, m->data);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	m->offset2 = vector3(0, 0, 0);
	if (!render(m))
	{
		ft_putstr_fd("ERROR\n", 2);
		exit_fdf(m);
	}
	return (0);
}

int			keypress_m(t_mlx *m)
{
	(void)m;
	if (m->mode == MODE_ISO)
		m->mode = MODE_PARALLEL;
	else if (m->mode == MODE_PARALLEL)
		m->mode = MODE_ISO;
	keypress_space(m);
	if (!render(m))
	{
		ft_putstr_fd("ERROR\n", 2);
		exit_fdf(m);
	}
	return (0);
}

int			keypress_r_f_t_g(t_mlx *m, int key)
{
	if (key == R_KEY_LINUX)
		m->scaleZneg = m->scaleZneg == 1 ? -1 : m->scaleZneg - 1;
	if (key == F_KEY_LINUX)
		m->scaleZneg = m->scaleZneg == -1 ? 1 : m->scaleZneg + 1;
	if (key == G_KEY_LINUX)
		m->scaleZ = m->scaleZ == 1 ? -1 : m->scaleZ - 1;
	if (key == T_KEY_LINUX)
		m->scaleZ = m->scaleZ == -1 ? 1 : m->scaleZ + 1;
	zero_image(m, m->data);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	if (!render(m))
	{
		ft_putstr_fd("ERROR\n", 2);
		exit_fdf(m);
	}
	return (0);
}

int			keypress_reset(t_mlx *m)
{
	m->scaleZneg = 1;
	m->scaleZ = 1;
	zero_image(m, m->data);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	if (!render(m))
	{
		ft_putstr_fd("ERROR\n", 2);
		exit_fdf(m);
	}
	return (0);
}