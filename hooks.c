/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 15:59:47 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	keypress(int key, void *param)
{
	t_mlx	*m;

	m = param;
	if (key == ESC_KEY_LINUX)
		keypress_esc(m);
	if (key == SP_KEY_LINUX)
		keypress_space(m);
	if (key == M_KEY_LINUX)
		keypress_m(m);
	if (key == O_KEY_LINUX || key == P_KEY_LINUX)
		keypress_o_p(m, key);
	if (key == R_KEY_LINUX || key == F_KEY_LINUX ||
		key == T_KEY_LINUX || key == G_KEY_LINUX)
		keypress_r_f_t_g(m, key);
	if (key == I_KEY_LINUX || key == K_KEY_LINUX
		|| key == J_KEY_LINUX || key == L_KEY_LINUX)
		keypress_i_j_k_l(m, key);
	if (key == N_KEY_LINUX)
		keypress_reset(m);
	return (0);
}

int			exit_fdf(t_mlx *m)
{
	clean_t_mlx(m);
	close(0);
	exit(0);
	return (0);
}

void		set_hooks(t_mlx *m)
{
	mlx_hook(m->win_ptr, 2, (1L << 0), keypress, (void *)m);
}
