/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:11:30 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "mlx.h"
# include "mlx_keycodes.h"
# include "libft.h"
# include "get_next_line.h"
# include <math.h>
# include <stdlib.h>
# include <fcntl.h>

# define V 1200
# define H 700
# define TITLE "Fdf"

# define FT_ABS(x) ((x) < 0 ? -(x) : (x))

# define MODE_ISO 1
# define MODE_PARALLEL 2

typedef unsigned	t_uint;

typedef struct		s_vector3
{
	int				x;
	int				y;
	int				z;
}					t_v3;

typedef struct		s_vector2
{
	int				x;
	int				y;
}					t_v2;

/*
** t_rc : is used to read file and store data
**	line : passed to get_next_line
**	numbers : used to store ints parsed from the input
**	w : amount of vectors by line (also the amount of splits)
**	h : amount of line
**	tab[h][w]
*/

typedef struct		s_read_container
{
	char			*line;
	int				*numbers;
	t_v3			**tab;
	int				h;
	int				w;
}					t_rc;

/*
** t_vc : is used to store our vectors
**	h : amount of line
**	w : amount of vector by line
**	tab[h][w]
*/

typedef struct		s_vector_container
{
	t_v3			**tab;
	int				h;
	int				w;
}					t_vc;

typedef struct		s_mlx
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img;
	char			*data;
	unsigned int	*data_32;
	int				width;
	int				height;
	int				bpp;
	int				sizeline;
	int				endian;
	unsigned int	color;
	char			*data_color;
	t_vc			*vc;
	t_v3			offset;
	t_v3			offset2;
	t_v3			step;
	int				scale;
	int				scaleZ;
	int				scaleZneg;
	int				mode;
}					t_mlx;

/*
** MISC
*/

int					min_int(int a, int b, int c, int d);
int					max_int(int a, int b, int c, int d);
int					atoi_split(t_rc *rc, char **split);
void				set_scale(t_mlx *m);
int					numbers_to_v3(t_rc *rc);

/*
** VECTOR3
*/

t_v3				vector3(int x, int y, int z);
void				swap(t_v3 *v1, t_v3 *v2);
t_v3				vector3(int x, int y, int z);
t_v3				mul(t_v3 v1, t_v3 v2);
t_v3				mul_scalar(t_v3 v1, int i);
t_v3				add(t_v3 v1, t_v3 v2);
t_v3				add_scalar(t_v3 v, int i);
t_v3				sub(t_v3 v1, t_v3 v2);
t_v3				sub_scalar(t_v3 v1, int i);
t_v3				to_iso(t_v3 v, int scale, int scaleZ, int scaleZneg);
t_v3				to_parallel(t_v3 v, int scale, int scaleZ, int scaleZneg);

/*
** T_RC - read container
*/

void				*clean_rc(t_rc *rc);
int					expand_tab(t_rc *rc);
t_rc				*malloc_rc(void);
void				*clean_rc(t_rc *rc);

/*
** T_VC - vector container
*/

t_vc				*malloc_vc(void);
void				*clean_vc(t_vc *vc);

/*
** T_MLX
*/

void				set_offset_step(t_mlx *m, t_v3 *varray);
void				*clean_t_mlx(t_mlx *m);
t_mlx				*malloc_mlx(void);

/*
** CLEAN
*/

int					clean_split(char **split);
int					clean_rc_split(t_rc *rc, char **split);

/*
** INPUT_CHECK
*/

size_t				nb_split(char const *s, char c);
int					is_valid(char *str);

/*
** READ
*/

t_mlx				*get_mlx(int fd);

/*
** DRAW
*/

void				zero_image(t_mlx *m, void *data);
void				bresenham(void *data, t_v3 from, t_v3 to);

/*
** RENDER
*/

void				iso_3d_points_to_2d(t_mlx *m, t_v3 *varray, int h);
void				set_offset_step(t_mlx *m, t_v3 *varray);
void				draw_varray(t_mlx *m, t_v3 *varray);
void				parallel_3d_points_to_2d(t_mlx *m, t_v3 *varray, int h);
int					render(t_mlx *m);

/*
** HOOKS
*/

int					keypress_esc(t_mlx *m);
int					keypress_space(t_mlx *m);
int					keypress_m(t_mlx *m);
int					keypress_i_j_k_l(t_mlx *m, int key);
int					keypress_o_p(t_mlx *m, int key);
int					keypress_r_f_t_g(t_mlx *m, int key);
int					keypress_reset(t_mlx *m);

int					exit_fdf(t_mlx *m);
void				set_hooks(t_mlx *m);

#endif
