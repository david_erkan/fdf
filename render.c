/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:05:11 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** Fills varray with all the v3 inside t_mlx::tab and transforms them into
** 2d points for the isometric rendering
*/

void		iso_3d_points_to_2d(t_mlx *m, t_v3 *varray, int h)
{
	int		pos;
	int		j;

	j = 0;
	while (j < m->vc->w)
	{
		pos = (h * m->vc->w) + j;
		varray[pos] = to_iso(varray[pos], m->scale, m->scaleZ, m->scaleZneg);
		++j;
	}
}

/*
** Fills varray with all the v3 inside t_mlx::tab and transforms them into
** 2d points for the isometric rendering
*/

void		parallel_3d_points_to_2d(t_mlx *m, t_v3 *varray, int h)
{
	int		pos;
	int		j;

	j = 0;
	while (j < m->vc->w)
	{
		pos = (h * m->vc->w) + j;
		varray[pos] = to_parallel(varray[pos], m->scale, m->scaleZ, m->scaleZneg);
		++j;
	}
}

void		draw_varray(t_mlx *m, t_v3 *varray)
{
	int		x;
	int		y;
	int		pos;
	t_v3	tmp;

	tmp = add(m->offset, m->offset2);
	y = 0;
	while (y < m->vc->h)
	{
		x = 0;
		while (x < m->vc->w)
		{
			pos = y * m->vc->w + x;
			if (x != 0)
				bresenham(m->data, add(varray[pos], tmp),
					add(varray[pos - 1], tmp));
			if (y != 0)
				bresenham(m->data, add(varray[pos], tmp),
					add(varray[pos - m->vc->w], tmp));
			++x;
		}
		++y;
	}
}

