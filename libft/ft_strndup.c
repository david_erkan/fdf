/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:56:06 by derkan            #+#    #+#             */
/*   Updated: 2018/12/05 12:59:16 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s, size_t n)
{
	size_t	size;
	char	*str;

	size = ft_strlen(s);
	if (size < n)
		return (NULL);
	str = (char *)malloc(n + 1);
	if (!str)
		return (NULL);
	ft_memcpy((void *)str, (const void *)s, n);
	str[n] = '\0';
	return (str);
}
