/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:49:43 by derkan            #+#    #+#             */
/*   Updated: 2018/12/04 15:41:30 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	const t_byte *p1 = (const t_byte *)s1;
	const t_byte *p2 = (const t_byte *)s2;

	while (*p1 != '\0')
	{
		if (*p2 != *p1)
			return ((int)(*p1 - *p2));
		p1++;
		p2++;
	}
	return (*p2 != '\0' ? -1 : 0);
}
