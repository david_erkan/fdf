/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:42:35 by derkan            #+#    #+#             */
/*   Updated: 2018/10/24 14:09:59 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	t_byte	*it;
	size_t	i;

	i = 0;
	it = (t_byte *)s;
	while (i < n)
		it[i++] = (t_byte)c;
	return (s);
}
