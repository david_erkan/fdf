/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:02:51 by derkan            #+#    #+#             */
/*   Updated: 2018/12/05 11:38:13 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*pnode;
	t_list	*tmp;

	if (!del || !alst)
		return ;
	pnode = *alst;
	while (pnode)
	{
		tmp = pnode;
		pnode = pnode->next;
		del(tmp->content, tmp->content_size);
		if (tmp)
			free(tmp);
		tmp = NULL;
	}
	*alst = NULL;
}
