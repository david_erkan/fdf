/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_del.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:02:51 by derkan            #+#    #+#             */
/*   Updated: 2018/12/05 12:36:17 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin_del(char *s1, char *s2, int flag)
{
	char	*tmp;

	if (!s1 || !(*s1) || !s2)
		return (NULL);
	tmp = ft_strjoin(s1, s2);
	if (flag & 1)
		free(s1);
	if (flag & 2)
		free(s2);
	return (tmp);
}
