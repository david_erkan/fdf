/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:04:24 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			min_int(int a, int b, int c, int d)
{
	int min;

	min = a < b ? a : b;
	min = min < c ? min : c;
	min = min < d ? min : d;
	return (min);
}

int			max_int(int a, int b, int c, int d)
{
	int max;

	max = a > b ? a : b;
	max = max > c ? max : c;
	max = max > d ? max : d;
	return (max);
}

/*
** Calls ft_atoi on each string inside split
** then stores the result inside t_rc::numbers
*/

int			atoi_split(t_rc *rc, char **split)
{
	int		i;

	i = 0;
	while (i < rc->w)
	{
		rc->numbers[i] = ft_atoi(split[i]);
		++i;
	}
	return (1);
}

/*
** Sets the scale with a simple calcul
*/

void		set_scale(t_mlx *m)
{
	int		dim;

	dim = H < V ? H : V;
	m->scale = dim / (m->vc->h > m->vc->w ? m->vc->h : m->vc->w);
}

/*
** from an array of int, fills t_rc::tab with the right values
*/

int			numbers_to_v3(t_rc *rc)
{
	int		i;
	int		h;

	i = 0;
	h = rc->h - 1;
	if ((rc->tab[h] = malloc(sizeof(t_v3) * rc->w)) == NULL)
		return (0);
	while (i < rc->w)
	{
		rc->tab[h][i].z = rc->numbers[i];
		rc->tab[h][i].x = i;
		rc->tab[h][i].y = h;
		++i;
	}
	return (1);
}
