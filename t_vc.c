/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_vc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:08:09 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_vc		*malloc_vc(void)
{
	t_vc	*vc;

	vc = malloc(sizeof(t_vc));
	if (!vc)
		return (NULL);
	vc->tab = NULL;
	vc->h = 0;
	vc->w = 0;
	return (vc);
}

void		*clean_vc(t_vc *vc)
{
	int		i;

	i = 0;
	if (!vc)
		return (NULL);
	if (vc->tab)
	{
		while (i < vc->h)
			free(vc->tab[i++]);
		free(vc->tab);
		vc->tab = NULL;
	}
	ft_memdel((void **)&vc);
	return (NULL);
}
