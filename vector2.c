/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:08:39 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_v3		add_scalar(t_v3 v, int i)
{
	v.x += i;
	v.y += i;
	v.z += i;
	return (v);
}

t_v3		sub(t_v3 v1, t_v3 v2)
{
	v1.x -= v2.x;
	v1.y -= v2.y;
	v1.z -= v2.z;
	return (v1);
}

t_v3		sub_scalar(t_v3 v1, int i)
{
	v1.x -= i;
	v1.y -= i;
	v1.z -= i;
	return (v1);
}

t_v3		to_iso(t_v3 v, int scale, int scaleZ, int scaleZneg)
{
	int		x;
	int		y;
	// int		delta;

	v.x *= scale;
	v.y *= scale;

	// delta = v.z * scaleZ - v.z;
	// v.z += FT_ABS(delta);

	// v.z += (scaleZ * 16);
	// v.z *= scaleZ;
	
	if (v.z > 0)
		v.z *= scaleZ;
	else if (v.z < 0)
		v.z *= scaleZneg;
	
	x = (v.x / 2) - v.y;
	y = -v.z + (v.x / 2) + (v.y / 2);
	v.x = x;
	v.y = y;
	return (v);
}

t_v3		to_parallel(t_v3 v, int scale, int scaleZ, int scaleZneg)
{
	int		x;
	int		y;
	// int		delta;

	v.x *= scale;
	v.y *= scale;

	// delta = v.z * scaleZ - v.z;
	// v.z += FT_ABS(delta);

	if (v.z > 0)
		v.z *= scaleZ;
	else if (v.z < 0)
		v.z *= scaleZneg;

	x = v.x + v.z / 2;
	y = v.y + -(v.z / 4);
	v.x = x;
	v.y = y;
	return (v);
}
