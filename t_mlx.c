/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_mlx.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:07:34 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			*clean_t_mlx(t_mlx *m)
{
	if (!m)
		return (NULL);
	if (m->win_ptr)
		mlx_destroy_window(m->mlx_ptr, m->win_ptr);
	if (m->img)
		mlx_destroy_image(m->mlx_ptr, m->img);
	clean_vc(m->vc);
	free(m);
	return (NULL);
}

static t_mlx	*new_mlx(void)
{
	t_mlx		*m;

	m = malloc(sizeof(t_mlx));
	if (!m)
		return (NULL);
	m->mlx_ptr = NULL;
	m->win_ptr = NULL;
	m->img = NULL;
	m->data = NULL;
	m->data_32 = NULL;
	m->vc = NULL;
	m->width = V;
	m->height = H;
	m->offset = vector3(0, 0, 0);
	m->offset2 = vector3(0, 0, 0);
	m->step = vector3(0, 0, 0);
	m->scale = 1;
	m->scaleZ = 1;
	m->scaleZneg = 1;
	m->mode = MODE_ISO;
	return (m);
}

t_mlx			*malloc_mlx(void)
{
	t_mlx		*m;

	m = new_mlx();
	if (!m)
		return (NULL);
	if (!(m->mlx_ptr = mlx_init()))
		return (clean_t_mlx(m));
	if (!((m->win_ptr = mlx_new_window(m->mlx_ptr, V, H, TITLE))))
		return ((void *)clean_t_mlx(m));
	if (!(m->img = mlx_new_image(m->mlx_ptr, V, H)))
		return ((void *)clean_t_mlx(m));
	if (!(m->data =
			mlx_get_data_addr(m->img, &m->bpp, &m->sizeline, &m->endian)))
		return ((void *)clean_t_mlx(m));
	m->data_32 = (t_uint *)m->data;
	return (m);
}

/*
** Parses the varray, finds the correct offset for the projection to be centered
*/

void			set_offset_step(t_mlx *m, t_v3 *varray)
{
	int			i;
	int			len;
	t_v3		max;
	t_v3		min;

	max.x = varray[0].x;
	max.y = varray[0].y;
	min.x = max.x;
	min.y = max.y;
	i = 0;
	len = m->vc->w * m->vc->h;
	while (i < len)
	{
		max.x = max.x < varray[i].x ? varray[i].x : max.x;
		max.y = max.y < varray[i].y ? varray[i].y : max.y;
		min.x = min.x > varray[i].x ? varray[i].x : min.x;
		min.y = min.y > varray[i].y ? varray[i].y : min.y;
		++i;
	}
	m->offset.x = V / 2 - (max.x - min.x) / 2 - min.x;
	m->offset.y = H / 2 - (max.y - min.y) / 2 - min.y;
	m->step.x = (max.x - min.x) / 7 < 10 ? 10 : (max.x - min.x) / 8;
	m->step.y = (max.y - min.y) / 7 < 10 ? 10 : (max.y - min.y) / 8;
	m->step.x = m->step.x > V / 3 ? V / 10 : m->step.x;
	m->step.y = m->step.y > H / 3 ? H / 10 : m->step.y;
}
