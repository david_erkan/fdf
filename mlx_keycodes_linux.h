/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_keycodes_linux.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 18:18:37 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 18:18:52 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MLX_KEYCODE_LINUX_H
# define MLX_KEYCODE_LINUX_H

/*
******************************************************************
** EVENT DEFINITIONS
******************************************************************
*/

/*
** Input Event Masks. Used as event-mask window attribute and as arguments
** to Grab requests.  Not to be confused with event names.
*/

# define NOEVENTMASK 0L
# define KPMASK (1L<<0)
# define KEYRELEASEMASK (1L<<1)
# define BUTTONPRESSMASK (1L<<2)
# define BUTTONRELEASEMASK (1L<<3)
# define ENTERWINDOWMASK (1L<<4)
# define LEAVEWINDOWMASK (1L<<5)
# define POINTERMOTIONMASK (1L<<6)
# define POINTERMOTIONHINTMASK (1L<<7)
# define BUTTON1MOTIONMASK (1L<<8)
# define BUTTON2MOTIONMASK (1L<<9)
# define BUTTON3MOTIONMASK (1L<<10)
# define BUTTON4MOTIONMASK (1L<<11)
# define BUTTON5MOTIONMASK (1L<<12)
# define BUTTONMOTIONMASK (1L<<13)
# define KEYMAPSTATEMASK (1L<<14)
# define EXPOSUREMASK (1L<<15)
# define VISIBILITYCHANGEMASK (1L<<16)
# define STRUCTURENOTIFYMASK (1L<<17)
# define RESIZEREDIRECTMASK (1L<<18)
# define SUBSTRUCTURENOTIFYMASK (1L<<19)
# define SUBSTRUCTUREREDIRECTMASK (1L<<20)
# define FOCUSCHANGEMASK (1L<<21)
# define PROPERTYCHANGEMASK (1L<<22)
# define COLORMAPCHANGEMASK (1L<<23)
# define OWNERGRABBUTTONMASK (1L<<24)

/* 
** Event names.  Used in "type" field in XEvent structures.  Not to be
** confused with event masks above.  They start from 2 because 0 and 1
** are reserved in the protocol for errors and replies.
*/

# define KEYPRESS 2
# define KEYRELEASE 3
# define BUTTONPRESS 4
# define BUTTONRELEASE 5
# define MOTIONNOTIFY 6
# define ENTERNOTIFY 7
# define LEAVENOTIFY 8
# define FOCUSIN 9
# define FOCUSOUT 10
# define KEYMAPNOTIFY 11
# define EXPOSE 12
# define GRAPHICSEXPOSE 13
# define NOEXPOSE 14
# define VISIBILITYNOTIFY 15
# define CREATENOTIFY 16
# define DESTROYNOTIFY 17
# define UNMAPNOTIFY 18
# define MAPNOTIFY 19
# define MAPREQUEST 20
# define REPARENTNOTIFY 21
# define CONFIGURENOTIFY 22
# define CONFIGUREREQUEST 23
# define GRAVITYNOTIFY 24
# define RESIZEREQUEST 25
# define CIRCULATENOTIFY 26
# define CIRCULATEREQUEST 27
# define PROPERTYNOTIFY 28
# define SELECTIONCLEAR 29
# define SELECTIONREQUEST 30
# define SELECTIONNOTIFY 31
# define COLORMAPNOTIFY 32
# define CLIENTMESSAGE 33
# define MAPPINGNOTIFY 34
# define GENERICEVENT 35
/* 
** must be bigger than any event # 
*/
# define LASTEVENT 36


/* 
** Key masks. Used as modifiers to GrabButton and GrabKey, 
** results of QueryPointer,
** state in various key-, mouse-, and button-related events.
*/

# define SHIFTMASK (1<<0)
# define LOCKMASK (1<<1)
# define CONTROLMASK (1<<2)
# define MOD1MASK (1<<3)
# define MOD2MASK (1<<4)
# define MOD3MASK (1<<5)
# define MOD4MASK (1<<6)
# define MOD5MASK (1<<7)

/* 
** modifier names.  Used to build a SetModifierMapping request or
** to read a GetModifierMapping request.  These correspond to the
** masks defined above.
*/
# define SHIFTMAPINDEX 0
# define LOCKMAPINDEX 1
# define CONTROLMAPINDEX 2
# define MOD1MAPINDEX 3
# define MOD2MAPINDEX 4
# define MOD3MAPINDEX 5
# define MOD4MAPINDEX 6
# define MOD5MAPINDEX 7


/* 
** button masks.  Used in same manner as Key masks above. Not to be confused
** with button names below.
*/

# define BUTTON1MASK (1<<8)
# define BUTTON2MASK (1<<9)
# define BUTTON3MASK (1<<10)
# define BUTTON4MASK (1<<11)
# define BUTTON5MASK (1<<12)

/* 
** used in GrabButton, GrabKey 
*/

# define ANYMODIFIER (1<<15)


/* 
** button names. Used as arguments to GrabButton and as detail in ButtonPress
** and ButtonRelease events.  Not to be confused with button masks above.
** Note that 0 is already defined above as "AnyButton".
*/


# define BUTTON1 1
# define BUTTON2 2
# define BUTTON3 3

/*
** button 4 - should be scroll up key
** button 5 - should be scroll down key 
** button 6 - //	//	//	//  left key
** button 7 - //	//	//	//  right key
*/

# define BUTTON4 4
# define BUTTON5 5

/*
** these two values work on the mac machines at school 19
** i'm not sure they'll work with other machines
*/

# define BUTTON6 6
# define BUTTON7 7

/*
** Notify modes
*/

# define NOTIFYNORMAL 0
# define NOTIFYGRAB 1
# define NOTIFYUNGRAB 2
# define NOTIFYWHILEGRABBED 3

/*
** for MotionNotify events
*/

# define NOTIFYHINT 1

/*
** Notify detail
*/

# define NOTIFYANCESTOR 0
# define NOTIFYVIRTUAL 1
# define NOTIFYINFERIOR 2
# define NOTIFYNONLINEAR 3
# define NOTIFYNONLINEARVIRTUAL 4
# define NOTIFYPOINTER 5
# define NOTIFYPOINTERROOT 6
# define NOTIFYDETAILNONE 7

/* 
** Visibility notify 
*/

# define VISIBILITYUNOBSCURED 0
# define VISIBILITYPARTIALLYOBSCURED 1
# define VISIBILITYFULLYOBSCURED 2

/* 
** Circulation request 
*/

# define PLACEONTOP 0
# define PLACEONBOTTOM 1

/* 
** protocol families 
*/

/* 
** IPv4 
*/

# define FAMILYINTERNET 0
# define FAMILYDECNET 1
# define FAMILYCHAOS 2

/* 
** IPv6 
*/

# define FAMILYINTERNET6 6

/* 
** authentication families not tied to a specific protocol 
*/

# define FAMILYSERVERINTERPRETED 5

/* 
** Property notification 
*/

# define PROPERTYNEWVALUE 0
# define PROPERTYDELETE 1

/* 
** Color Map notification 
*/

# define COLORMAPUNINSTALLED 0
# define COLORMAPINSTALLED 1

/* 
** GrabPointer, GrabButton, GrabKeyboard, GrabKey Modes 
*/

# define GRABMODESYNC 0
# define GRABMODEASYNC 1

/* 
** GrabPointer, GrabKeyboard reply status 
*/

# define GRABSUCCESS 0
# define ALREADYGRABBED 1
# define GRABINVALIDTIME 2
# define GRABNOTVIEWABLE 3
# define GRABFROZEN 4

/* 
** AllowEvents modes 
*/

# define ASYNCPOINTER 0
# define SYNCPOINTER 1
# define REPLAYPOINTER 2
# define ASYNCKEYBOARD 3
# define SYNCKEYBOARD 4
# define REPLAYKEYBOARD 5
# define ASYNCBOTH 6
# define SYNCBOTH 7

/* 
** Used in SetInputFocus, GetInputFocus 
*/

# define REVERTTONONE (int)None
# define REVERTTOPOINTERROOT (int)PointerRoot
# define REVERTTOPARENT 2

/*
****************************************************************
** ERROR CODES
****************************************************************
*/

/*
** everything's okay 
*/
	
# define SUCCESS 0

/*
** bad request code 
*/
	
# define BADREQUEST 1

/*
** int parameter out of range 
*/
	
# define BADVALUE 2

/*
** parameter not a Window 
*/
	
# define BADWINDOW 3

/*
** parameter not a Pixmap 
*/
	
# define BADPIXMAP 4

/*
** parameter not an Atom 
*/
	
# define BADATOM 5

/*
** parameter not a Cursor 
*/
	
# define BADCURSOR 6

/*
** parameter not a Font 
*/
	
# define BADFONT 7

/*
** parameter mismatch 
*/
	
# define BADMATCH 8
	
/*
** parameter not a Pixmap or Window 
*/

# define BADDRAWABLE 9

# define BADACCESS 10	

/* depending on context:
**  - key/button already grabbed
**  - attempt to free an illegal
**  cmap entry
**  - attempt to store into a read-only
**  color map entry.
**   - attempt to modify the access control
**  list from other than the local host.
*/

/* 
** insufficient resources 
*/

# define BADALLOC 11

/* 
** no such colormap 
*/

# define BADCOLOR 12

/* 
** parameter not a GC 
*/

# define BADGC 13

/* 
** choice not in range or already used 
*/

# define BADIDCHOICE 14

/* 
** font or color name doesn't exist 
*/

# define BADNAME 15

/* 
** Request length incorrect 
*/

# define BADLENGTH 16

/* 
** server is defective 
*/

# define BADIMPLEMENTATION 17

# define FIRSTEXTENSIONERROR	128
# define LASTEXTENSIONERROR	255

/*
****************************************************************
** WINDOW DEFINITIONS
****************************************************************
*/

/* 
** Window classes used by CreateWindow 
*/

/* 
** Note that CopyFromParent is already defined as 0 above 
*/

# define INPUTOUTPUT 1
# define INPUTONLY 2

/* 
** Window attributes for CreateWindow and ChangeWindowAttributes 
*/

# define CWBACKPIXMAP (1L<<0)
# define CWBACKPIXEL (1L<<1)
# define CWBORDERPIXMAP (1L<<2)
# define CWBORDERPIXEL (1L<<3)
# define CWBITGRAVITY (1L<<4)
# define CWWINGRAVITY (1L<<5)
# define CWBACKINGSTORE (1L<<6)
# define CWBACKINGPLANES (1L<<7)
# define CWBACKINGPIXEL (1L<<8)
# define CWOVERRIDEREDIRECT	(1L<<9)
# define CWSAVEUNDER (1L<<10)
# define CWEVENTMASK (1L<<11)
# define CWDONTPROPAGATE (1L<<12)
# define CWCOLORMAP (1L<<13)
# define CWCURSOR (1L<<14)

/* 
** ConfigureWindow structure 
*/

# define CWX (1<<0)
# define CWY (1<<1)
# define CWWIDTH (1<<2)
# define CWHEIGHT (1<<3)
# define CWBORDERWIDTH (1<<4)
# define CWSIBLING (1<<5)
# define CWSTACKMODE (1<<6)


/* 
Bit Gravity 
*/

# define FORGETGRAVITY 0
# define NORTHWESTGRAVITY 1
# define NORTHGRAVITY 2
# define NORTHEASTGRAVITY 3
# define WESTGRAVITY 4
# define CENTERGRAVITY 5
# define EASTGRAVITY 6
# define SOUTHWESTGRAVITY 7
# define SOUTHGRAVITY 8
# define SOUTHEASTGRAVITY 9
# define STATICGRAVITY 10

/*
** Window gravity + bit gravity above 
*/

# define UNMAPGRAVITY 0

/* 
** Used in CreateWindow for backing-store hint 
*/

# define NOTUSEFUL 0
# define WHENMAPPED 1
# define ALWAYS 2

/* 
** Used in GetWindowAttributes reply 
*/

# define ISUNMAPPED 0
# define ISUNVIEWABLE 1
# define ISVIEWABLE 2

/* 
** Used in ChangeSaveSet 
*/

# define SETMODEINSERT 0
# define SETMODEDELETE 1

/* 
** Used in ChangeCloseDownMode 
*/

# define DESTROYALL 0
# define RETAINPERMANENT 1
# define RETAINTEMPORARY 2

/* 
** Window stacking method (in configureWindow) 
*/

# define ABOVE 0
# define BELOW 1
# define TOPIF 2
# define BOTTOMIF 3
# define OPPOSITE 4

/* 
** Circulation direction 
*/

# define RAISELOWEST 0
# define LOWERHIGHEST 1

/* 
** Property modes 
*/

# define PROPMODEREPLACE 0
# define PROPMODEPREPEND 1
# define PROPMODEAPPEND 2

/*
****************************************************************
** GRAPHICS DEFINITIONS
****************************************************************
*/

/* 
** graphics functions, as in GC.alu 
*/

/* 
** 0 
*/

# define GXCLEAR 0x0

/* 
** src AND dst 
*/

# define GXAND 0x1

/* 
** src AND NOT dst 
*/

# define GXANDREVERSE 0x2

/* 
** src 
*/

# define GXCOPY 0x3

/* 
** NOT src AND dst 
*/

# define GXANDINVERTED 0x4

/* 
** dst 
*/

# define GXNOOP 0x5

/* 
** src XOR dst 
*/

# define GXXOR 0x6

/* 
** src OR dst 
*/

# define GXOR 0x7

/* 
** NOT src AND NOT dst 
*/

# define GXNOR 0x8

/* 
** NOT src XOR dst 
*/

# define GXEQUIV 0x9

/* 
** NOT dst 
*/

# define GXINVERT 0xa

/* 
** src OR NOT dst 
*/

# define GXORREVERSE 0xb

/* 
** NOT src 
*/

# define GXCOPYINVERTED 0xc

/* 
** NOT src OR dst 
*/

# define GXORINVERTED 0xd

/* 
** NOT src OR NOT dst 
*/

# define GXNAND 0xe

/* 
** 1 
*/

# define GXSET 0xf

/* 
** LineStyle 
*/

# define LINESOLID 0
# define LINEONOFFDASH 1
# define LINEDOUBLEDASH 2

/* 
** capStyle 
*/

# define CAPNOTLAST 0
# define CAPBUTT 1
# define CAPROUND 2
# define CAPPROJECTING 3

/* 
** joinStyle 
*/

# define JOINMITER 0
# define JOINROUND 1
# define JOINBEVEL 2

/* 
** fillStyle 
*/

# define FILLSOLID 0
# define FILLTILED 1
# define FILLSTIPPLED 2
# define FILLOPAQUESTIPPLED	3

/* 
** fillRule 
*/

# define EVENODDRULE 0
# define WINDINGRULE 1

/* 
** subwindow mode 
*/

# define CLIPBYCHILDREN 0
# define INCLUDEINFERIORS	1

/* 
** SetClipRectangles ordering 
*/

# define UNSORTED 0
# define YSORTED 1
# define YXSORTED 2
# define YXBANDED 3

/* 
** CoordinateMode for drawing routines 
*/

/*
** relative to the origin 
*/

# define COORDMODEORIGIN 0

/*
** relative to previous point
*/

# define COORDMODEPREVIOUS 1

/* 
** Polygon shapes 
*/

/*
** paths may intersect 
*/

# define COMPLEX 0

/*
** no paths intersect, but not convex 
*/

# define NONCONVEX 1

/*
** wholly convex 
*/

# define CONVEX 2

/* 
** Arc modes for PolyFillArc 
*/

/*
** join endpoints of arc 
*/

# define ARCCHORD 0

/*
** join endpoints to center of arc 
*/

# define ARCPIESLICE 1

/* 
** GC components: masks used in CreateGC, CopyGC, ChangeGC, OR'ed into
** GC.stateChanges 
*/

# define GCFUNCTION (1L<<0)
# define GCPLANEMASK (1L<<1)
# define GCFOREGROUND (1L<<2)
# define GCBACKGROUND (1L<<3)
# define GCLINEWIDTH (1L<<4)
# define GCLINESTYLE (1L<<5)
# define GCCAPSTYLE (1L<<6)
# define GCJOINSTYLE (1L<<7)
# define GCFILLSTYLE (1L<<8)
# define GCFILLRULE (1L<<9)
# define GCTILE (1L<<10)
# define GCSTIPPLE (1L<<11)
# define GCTILESTIPXORIGIN (1L<<12)
# define GCTILESTIPYORIGIN (1L<<13)
# define GCFONT (1L<<14)
# define GCSUBWINDOWMODE (1L<<15)
# define GCGRAPHICSEXPOSURES (1L<<16)
# define GCCLIPXORIGIN (1L<<17)
# define GCCLIPYORIGIN (1L<<18)
# define GCCLIPMASK (1L<<19)
# define GCDASHOFFSET (1L<<20)
# define GCDASHLIST (1L<<21)
# define GCARCMODE (1L<<22)

# define GCLASTBIT 22
/*
****************************************************************
** FONTS
****************************************************************
*/

/* 
** used in QueryFont -- draw direction 
*/

# define FONTLEFTTORIGHT 0
# define FONTRIGHTTOLEFT 1

# define FONTCHANGE 255

/*
****************************************************************
**  IMAGING
****************************************************************
*/

/* 
** ImageFormat -- PutImage, GetImage 
*/

/*
** depth 1, XYFormat 
*/

# define XYBITMAP 0

/*
** depth == drawable depth 
*/

# define XYPIXMAP 1

/*
** depth == drawable depth 
*/

# define ZPIXMAP 2

/*
****************************************************************
**  COLOR MAP STUFF
****************************************************************
*/

/* 
** For CreateColormap 
*/

/*
** create map with no entries 
*/

# define ALLOCNONE 0

/*
** allocate entire map writeable 
*/

# define ALLOCALL 1

/* 
** Flags used in StoreNamedColor, StoreColors 
*/

# define DORED (1<<0)
# define DOGREEN (1<<1)
# define DOBLUE (1<<2)

/*
****************************************************************
** CURSOR STUFF
****************************************************************
*/

/* QueryBestSize Class */

/*
** largest size that can be displayed 
*/

# define CURSORSHAPE 0

/*
** size tiled fastest 
*/

# define TILESHAPE 1

/*
** size stippled fastest 
*/

# define STIPPLESHAPE 2

/*
****************************************************************
** KEYBOARD/POINTER STUFF
****************************************************************
*/

# define AUTOREPEATMODEOFF 0
# define AUTOREPEATMODEON 1
# define AUTOREPEATMODEDEFAULT 2

# define LEDMODEOFF 0
# define LEDMODEON 1

/* 
** masks for ChangeKeyboardControl 
*/

# define KBKEYCLICKPERCENT (1L<<0)
# define KBBELLPERCENT (1L<<1)
# define KBBELLPITCH (1L<<2)
# define KBBELLDURATION (1L<<3)
# define KBLED (1L<<4)
# define KBLEDMODE (1L<<5)
# define KBKEY (1L<<6)
# define KBAUTOREPEATMODE (1L<<7)

# define MAPPINGSUCCESS 0
# define MAPPINGBUSY 1
# define MAPPINGFAILED 2

# define MAPPINGMODIFIER 0
# define MAPPINGKEYBOARD 1
# define MAPPINGPOINTER 2

/*
****************************************************************
** SCREEN SAVER STUFF
****************************************************************
*/

# define DONTPREFERBLANKING 0
# define PREFERBLANKING 1
# define DEFAULTBLANKING 2

# define DISABLESCREENSAVER 0
# define DISABLESCREENINTERVAL 0

# define DONTALLOWEXPOSURES 0
# define ALLOWEXPOSURES 1
# define DEFAULTEXPOSURES 2

/* 
** for ForceScreenSaver 
*/

# define SCREENSAVERRESET 0
# define SCREENSAVERACTIVE 1

/*
****************************************************************
** HOSTS AND CONNECTIONS
****************************************************************
*/

/* 
** for ChangeHosts 
*/

# define HOSTINSERT 0
# define HOSTDELETE 1

/* 
** for ChangeAccessControl 
*/

# define ENABLEACCESS 1
# define DISABLEACCESS 0

/* 
** Display classes  used in opening the connection
** Note that the statically allocated ones are even numbered and the
** dynamically changeable ones are odd numbered 
*/

# define STATICGRAY 0
# define GRAYSCALE 1
# define STATICCOLOR 2
# define PSEUDOCOLOR 3
# define TRUECOLOR 4
# define DIRECTCOLOR 5


/* 
** Byte order  used in imageByteOrder and bitmapBitOrder 
*/

# define LSBFIRST 0
# define MSBFIRST 1

/*
** MAC_KEYBOARD
*/

# define A_KEY 0
# define B_KEY 11
# define C_KEY 8
# define D_KEY 2
# define E_KEY 14
# define F_KEY 3
# define G_KEY 5
# define H_KEY 4
# define I_KEY 34
# define J_KEY 38
# define K_KEY 40
# define L_KEY 37
# define M_KEY 46
# define N_KEY 45
# define O_KEY 31
# define P_KEY 35
# define Q_KEY 12
# define R_KEY 15
# define S_KEY 1
# define T_KEY 17
# define U_KEY 32
# define V_KEY 9
# define W_KEY 13
# define X_KEY 7
# define Y_KEY 16
# define Z_KEY 6

# define K1_KEY 18
# define K2_KEY 19
# define K3_KEY 20
# define K4_KEY 21
# define K5_KEY 23
# define K6_KEY 22
# define K7_KEY 26
# define K8_KEY 28
# define K9_KEY 25
# define K0_KEY 29

# define SP_KEY 49
# define BQ_KEY 50
# define MN_KEY 27
# define PL_KEY 24
# define SBO_KEY 33
# define SBC_KEY 30
# define BSL_KEY 42
# define SC_KEY 41
# define SQ_KEY 39
# define CM_KEY 43
# define PT_KEY 47
# define SL_KEY 44

# define F1_KEY 122
# define F2_KEY 120
# define F3_KEY 99
# define F4_KEY 118
# define F5_KEY 96
# define F6_KEY 97
# define F7_KEY 98
# define F8_KEY 100
# define F9_KEY 101
# define F10_KEY 109
# define F11_KEY
# define F12_KEY 111
# define F13_KEY 105
# define F14_KEY 107
# define F15_KEY 113
# define F16_KEY 106
# define F17_KEY 64
# define F18_KEY 79
# define F19_KEY 80

# define UP_KEY 126
# define DOWN_KEY 125
# define RIGHT_KEY 124
# define LEFT_KEY 123

# define ESC_KEY 53
# define TAB_KEY 48
# define CL_KEY 272
# define LSFT_KEY 257
# define LCTRL_KEY 256
# define LOPT_KEY 261
# define LCMD_KEY 259
# define LDEL_KEY 51
# define RTN_KEY 36
# define RSFT_KEY 258
# define RCTRL_KEY 269
# define ROPT_KEY 262
# define RCMD_KEY 260
# define EJ_KEY
# define FN_KEY 279
# define RDEL_KEY 117
# define HOME_KEY 115
# define END_KEY 119
# define PUP_KEY 116
# define PDOWN_KEY 121
# define CLR_KEY 71

# define NK0_KEY 82
# define NK1_KEY 83
# define NK2_KEY 84
# define NK3_KEY 85
# define NK4_KEY 86
# define NK5_KEY 87
# define NK6_KEY 88
# define NK7_KEY 89
# define NK8_KEY 91
# define NK9_KEY 92
# define NKEQ_KEY 81
# define NKSL_KEY 75
# define NKWC_KEY 67
# define NKMN_KEY 78
# define NKPL_KEY 69
# define NKPT_KEY 65
# define NKNTR_KEY 76

/*
** LINUX_KEYBOARD
*/

# define KEY_ESC_LINUX 65307
# define KEY_SPACE_LINUX 32

# define KEY_A_LINUX 97
# define KEY_B_LINUX 98
# define KEY_C_LINUX 99
# define KEY_D_LINUX 100
# define KEY_E_LINUX 101
# define KEY_F_LINUX 102
# define KEY_G_LINUX 103
# define KEY_H_LINUX 104
# define KEY_I_LINUX 105
# define KEY_J_LINUX 106
# define KEY_K_LINUX 107
# define KEY_L_LINUX 108
# define KEY_M_LINUX 109
# define KEY_N_LINUX 110
# define KEY_O_LINUX 111
# define KEY_P_LINUX 112
# define KEY_Q_LINUX 113
# define KEY_R_LINUX 114
# define KEY_S_LINUX 115
# define KEY_T_LINUX 116
# define KEY_U_LINUX 117
# define KEY_V_LINUX 118
# define KEY_W_LINUX 119
# define KEY_X_LINUX 120
# define KEY_Y_LINUX 121
# define KEY_Z_LINUX 107

#endif
