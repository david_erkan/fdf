/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 12:37:00 by derkan            #+#    #+#             */
/*   Updated: 2019/02/04 15:58:06 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char			**get_content(t_list **lst, int const fd)
{
	t_list			*tmp;

	tmp = *lst;
	while (tmp)
	{
		if (tmp->content_size == (size_t)fd)
			return ((char**)&tmp->content);
		tmp = tmp->next;
	}
	if ((tmp = ft_lstnew(NULL, 0)) == NULL)
		return (NULL);
	tmp->content_size = (size_t)fd;
	if ((tmp->content = ft_strdup("")) == NULL)
	{
		ft_memdel((void **)&tmp);
		return (NULL);
	}
	tmp->next = *lst;
	*lst = tmp;
	return ((char**)&tmp->content);
}

static void			remove_node(int const fd, t_list **lst)
{
	t_list			*front;
	t_list			*back;

	if (!lst || !*lst || fd < 0)
		return ;
	front = (*lst)->next;
	if ((back = *lst) != NULL && back->content_size == (size_t)fd)
	{
		ft_memdel(&back->content);
		ft_memdel((void **)&back);
		*lst = front;
		return ;
	}
	while (front)
	{
		if (front->content_size == (size_t)fd)
		{
			back->next = front->next;
			ft_memdel(&front->content);
			ft_memdel((void **)&front);
			return ;
		}
		back = front;
		front = front->next;
	}
}

static int			get_line(char **leftover, char **line)
{
	char			*occ;
	char			*new;
	char			*tmp;
	size_t			len;

	occ = ft_strchr(*leftover, '\n');
	len = occ ? (size_t)(occ - *leftover) : (size_t)(ft_strlen(*leftover));
	new = ft_strnew(len);
	new = new ? ft_strncpy(new, *leftover, len) : new;
	new ? (void)new : ft_strdel(leftover);
	if (!new)
		return (0);
	tmp = new;
	new = ft_strsub(*leftover, len + 1, ft_strlen(*leftover + len));
	ft_strdel(leftover);
	*leftover = new;
	if (!new)
		return (0);
	*line = tmp;
	return (1);
}

static int			last_check(int const fd, t_list **lst,
						char **prev, char **line)
{
	if (*prev && **prev)
	{
		if (get_line(prev, line) == 0)
		{
			remove_node(fd, lst);
			return (-1);
		}
		return (1);
	}
	remove_node(fd, lst);
	return (0);
}

int					get_next_line(int const fd, char **line)
{
	static t_list	*lst;
	char			**prev;
	char			*tmp;
	char			buffer[BUFF_SIZE + 1];
	ssize_t			r;

	if (fd < 0 || !line || (prev = get_content(&lst, fd)) == NULL)
		return (-1);
	while (!ft_strchr(*prev, '\n') && (r = read(fd, buffer, BUFF_SIZE)))
	{
		buffer[r < 0 ? 0 : r] = '\0';
		if (r < 0 || !prev || !(*prev))
		{
			remove_node(fd, &lst);
			return (-1);
		}
		tmp = *prev;
		if ((*prev = ft_strjoin(*prev, buffer)) == NULL)
			remove_node(fd, &lst);
		free(tmp);
		if (!(*prev))
			return (-1);
	}
	return (last_check(fd, &lst, prev, line));
}
