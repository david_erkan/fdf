/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:13:20 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		zero_image(t_mlx *m, void *data)
{
	ft_memset(data, 0, (size_t)m->sizeline * H);
}

void		put_pixel(t_uint *data, t_v3 v, int color)
{
	if (v.x >= V || v.y >= H || v.x < 0 || v.y < 0)
		return ;
	data[v.y * V + v.x] = color;
}

/*
** is used in function circle to draw points in all eight octans
*/

/*
** static void	draw_points_circle(void *data, t_v3 s, t_v3 pos, int color)
** {
** 	put_pixel((t_uint *)data, vector3(s.x + pos.x, s.y + pos.y, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x + pos.y, s.y + pos.x, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x - pos.y, s.y + pos.x, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x - pos.x, s.y + pos.y, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x - pos.x, s.y - pos.y, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x - pos.y, s.y - pos.x, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x + pos.y, s.y - pos.x, 0), color);
** 	put_pixel((t_uint *)data, vector3(s.x + pos.x, s.y - pos.y, 0), color);
** }
*/

/*
** this circle function is optimized for integer operations
** makes use of function draw_points_circle to draw points in all octants
*/

/*
** void		circle(void *data, t_v3 v, int radius)
** {
**     int		dx;
**     int		dy;
**     int		err;
** 	t_v3	pos;
**
** 	pos.x = radius - 1;
**     pos.y = 0;
**     dx = 1;
**     dy = 1;
**     err = dx - (radius << 1);
**     while (pos.x >= pos.y)
**     {
** 		draw_points_circle(data, v, pos, 0xff0000);
** 		if (err <= 0)
**         {
**             pos.y++;
**             err += dy;
**             dy += 2;
**         }
**         if (err > 0)
**         {
**             pos.x--;
**             dx += 2;
**             err += dx - (radius << 1);
**         }
**     }
** }
*/

/*
** This function draws line from 'from' to 'to' and is optimized for
** integer operations
*/

void		bresenham(void *data, t_v3 from, t_v3 to)
{
	t_v3	dx_dy;
	t_v3	sx_sy;
	t_v3	err_e2;

	dx_dy = vector3(FT_ABS(to.x - from.x), FT_ABS(to.y - from.y), 0);
	sx_sy = vector3(from.x < to.x ? 1 : -1, from.y < to.y ? 1 : -1, 0);
	err_e2 = vector3((dx_dy.x > dx_dy.y ? dx_dy.x : -dx_dy.y) / 2, 0, 0);
	while (1)
	{
		put_pixel((t_uint *)data, from, 0x00ff00);
		if (from.x == to.x && from.y == to.y)
			break ;
		err_e2.y = err_e2.x;
		if (err_e2.y > -dx_dy.x)
		{
			err_e2.x -= dx_dy.y;
			from.x += sx_sy.x;
		}
		if (err_e2.y < dx_dy.y)
		{
			err_e2.x += dx_dy.x;
			from.y += sx_sy.y;
		}
	}
}
