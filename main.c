/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:04:10 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			main(int argc, char **argv)
{
	t_mlx	*m;
	int		fd;

	m = NULL;
	if (argc != 2)
	{
		ft_putstr("usage : fdf input_file\n");
		return (0);
	}
	if ((fd = open(argv[1], O_RDONLY)) <= 0)
	{
		ft_putstr_fd("ERROR\n", 2);
		return (EXIT_FAILURE);
	}
	if (!(m = get_mlx(fd)))
	{
		ft_putstr_fd("ERROR\n", 2);
		return (EXIT_FAILURE);
	}
	close(fd);
	if (!render(m))
		exit_fdf(m);
	mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
	mlx_loop(m->mlx_ptr);
	return (0);
}
