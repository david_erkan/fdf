/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:04:47 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			populate_once(t_rc *rc)
{
	char	**split;

	if ((split = ft_strsplit(rc->line, ' ')) == NULL)
		return (clean_split(split));
	if ((rc->numbers = malloc(sizeof(int) * rc->w)) == NULL)
		return (clean_split(split));
	atoi_split(rc, split);
	if ((rc->tab = malloc(sizeof(t_v3 *) * 1)) == NULL)
		return (clean_split(split));
	++rc->h;
	if ((numbers_to_v3(rc)) == 0)
		return (clean_split(split));
	clean_split(split);
	ft_strdel(&rc->line);
	return (1);
}

int			read_once(t_rc *rc, int fd)
{
	int		r;

	if ((r = get_next_line(fd, &rc->line)) == 0 || r == -1)
		return (0);
	if ((rc->w = nb_split(rc->line, ' ')) == 0)
		return (0);
	if (!is_valid(rc->line))
		return (0);
	if ((populate_once(rc)) == 0)
		return (0);
	return (1);
}

int			read_input(t_rc *rc, int fd)
{
	int		r;
	char	**split;

	split = NULL;
	while ((r = get_next_line(fd, &rc->line)))
	{
		if (r == -1)
			return (clean_split(split));
		if ((int)nb_split(rc->line, ' ') != rc->w || !is_valid(rc->line))
			return (clean_split(split));
		if ((split = ft_strsplit(rc->line, ' ')) == NULL)
			return (clean_split(split));
		atoi_split(rc, split);
		if (expand_tab(rc) == 0)
			return (clean_split(split));
		++rc->h;
		if ((numbers_to_v3(rc)) == 0)
			return (clean_split(split));
		ft_strdel(&rc->line);
		clean_split(split);
	}
	return (1);
}

t_vc		*return_input(int fd)
{
	t_rc	*rc;
	t_vc	*vc;

	if ((rc = malloc_rc()) == NULL)
		return (NULL);
	if ((read_once(rc, fd)) == 0 || (read_input(rc, fd)) == 0)
		return ((void *)clean_rc(rc));
	if ((vc = malloc_vc()) == NULL)
		return ((void *)clean_rc(rc));
	vc->h = rc->h;
	vc->w = rc->w;
	vc->tab = rc->tab;
	free(rc->numbers);
	free(rc);
	return (vc);
}

t_mlx		*get_mlx(int fd)
{
	t_vc	*vc;
	t_mlx	*m;

	if (!(vc = return_input(fd)))
		return (NULL);
	if (!(m = malloc_mlx()))
	{
		clean_vc(vc);
		return (NULL);
	}
	m->vc = vc;
	set_scale(m);
	set_hooks(m);
	return (m);
}
