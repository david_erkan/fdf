/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:08:25 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		swap(t_v3 *v1, t_v3 *v2)
{
	t_v3	temp;

	temp = *v1;
	*v1 = *v2;
	*v2 = temp;
}

t_v3		vector3(int x, int y, int z)
{
	t_v3	v;

	v.x = x;
	v.y = y;
	v.z = z;
	return (v);
}

t_v3		mul(t_v3 v1, t_v3 v2)
{
	if (v2.x == 0)
		return (v1);
	v1.x *= v2.x;
	if (v2.y == 0)
		return (v1);
	v1.y *= v2.y;
	if (v2.z == 0)
		return (v1);
	v1.z *= v2.z;
	return (v1);
}

t_v3		mul_scalar(t_v3 v1, int i)
{
	if (i == 0)
		return (v1);
	v1.x *= i;
	v1.y *= i;
	v1.z *= i;
	return (v1);
}

t_v3		add(t_v3 v1, t_v3 v2)
{
	v1.x += v2.x;
	v1.y += v2.y;
	v1.z += v2.z;
	return (v1);
}
