# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: derkan <derkan@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/09 13:12:28 by derkan            #+#    #+#              #
#    Updated: 2019/03/13 16:12:42 by derkan           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC=gcc
CFLAGS = -g -Wall -Wextra -Werror
INCLUDES_PATH = -I ./ -I ./libft -I ./minilibx_macos
LIBS_PATH = -L ./minilibx_macos -L ./libft
LIBS = -lmlx -lXext -lX11 -lft -lm
LIBS_MAC = $(FRAMEWORK)
EXEC = fdf
LIBMLX_LIB = minilibx_macos/libmlx.a
LIBFT_LIB = libft/libft.a
SRC = clean.c hooks.c keypresses2.c read.c t_vc.c draw.c input_check.c \
main.c render2.c t_mlx.c vector.c get_next_line.c keypresses.c misc.c \
render.c t_rc.c vector2.c
FRAMEWORK = -lft -lmlx -framework OpenGL -framework Appkit
OBJ = $(SRC:.c=.o)

.PHONY : all clean fclean re
all: $(EXEC)

$(EXEC): $(LIBFT_LIB) $(LIBMLX_LIB) $(OBJ)
	$(CC) -o $(EXEC) $(CFLAGS) $(OBJ) $(INCLUDES_PATH) $(LIBS_PATH) $(LIBS_MAC)

$(LIBFT_LIB):
	make -C ./libft

$(LIBMLX_LIB):
	make -C ./minilibx_macos

misc.o: misc.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

vector.o: vector.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

vector2.o: vector2.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

t_mlx.o: t_mlx.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

render2.o: render2.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

render.o: render.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

draw.o: draw.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

input_check.o: input_check.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

t_rc.o: t_rc.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

t_vc.o: t_vc.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

read.o: read.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

clean.o: clean.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

hooks.o: hooks.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

keypresses.o: keypresses.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

keypresses2.o: keypresses2.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

get_next_line.o: get_next_line.c get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

main.o: main.c fdf.h mlx_keycodes.h get_next_line.h libft/libft.h
	$(CC) -c $< $(CFLAGS) $(INCLUDES_PATH)

clean:
	rm -f $(OBJ)
	make -C ./libft clean
	make -C ./minilibx clean

fclean: clean
	rm -f $(OBJ)
	make -C ./libft fclean
	make -C ./minilibx fclean

re: fclean all
