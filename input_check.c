/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: derkan <derkan@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 13:04:22 by derkan            #+#    #+#             */
/*   Updated: 2019/03/04 16:02:51 by derkan           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
** returns the number of splits inside a string
*/

size_t		nb_split(char const *s, char c)
{
	char	*it;
	size_t	nb_split;

	if (!*s)
		return (0);
	it = (char *)s;
	nb_split = 0;
	while (*it)
	{
		if (it[0] != c && (it[1] == c || !it[1]))
			++nb_split;
		++it;
	}
	return (nb_split);
}

/*
** Checks if a string is only made of valid numbers
** It is ok if there is more than one space between each numbers
*/

int			is_valid(char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && str[i] != '-' && (str[i] < '0' || str[i] > '9'))
			return (0);
		if (str[i] == '-' && (str[i + 1] < '0' || str[i + 1] > '9'))
			return (0);
		++i;
	}
	return (1);
}
